---



# !!
# !!
# This site can be viewed at https://cv.acpatt.com
# !!
# !!



title: Full CV

# summary_title: Skillset
#summary: >
#  This is a list of my skillset, sorted by whether it's technical or non-technical, and what area of 'technical' it falls under.

#  Hi, I'm Andrew. In the last 5 years I've gone from achieving an undergraduate degree in Physics and Mathematics, to completing an internship focused on cybersecurity and InfoSec, to roles focused on server and network engineering, C\#/PHP programming and now IaC/Config-as-code and automation work. Alongside building skills within my employment, for the last four years my hobby has been creating and managing virtual infrastructure, but as I've become more skilled in containerisation, automation tools and the world of DevOps, I have found it to be a rewarding and exciting vocation in which I intend to pursue a career. 


sections:
  - heading: THIS CV IS OUT OF DATE!
    title: THIS WAS MY SKILLSET AS OF ~MID 2020. A LOT HAS CHANGED SINCE THEN
    description: |
      I am currently underway updating the documentation around my professional existence. Come back in early to mid 2023 for an update on who I am and what I can do.

      -----------------------------------------------------------------------------------------------
  - heading: Skills
    description: |
      This below is my skillset, sorted by whether it's technical or non-technical, and what area of 'technical' it falls under.
    accordion: 
      - title: Skill levels
        content: |
          Below are rough descriptions of what I mean when I say 'comfortable, confident, proficient etc, loosely based on the [NIH Competencies Proficiencies scale](https://hr.nih.gov/working-nih/competencies/competencies-proficiency-scale)
          * 'Some experience/Grounding in' - I've done this once or twice. I know roughly what's involved and could have a go, but will need to do some learning to get up to an operational level.
          * 'Experience in' - I've done this a few times. I know how to do the basics but I've not had to deal with anything too complex.
          * 'Competent/Comfortable' - I've done this a lot, I've a good idea of what i'm doing and I can solve most problems unaided (unless you count using stackexchange as aid).
          * 'Confident/Proficient' - I'd say this was a key skill. I'm the person in my organisation people come to for help with this.

  - title: Technical
    accordion: 
      - title: Programming & Scripting
        content: |
          * Grounding in Javascript and NodeJS development through **[collaborating on software projects](https://gitlab.com/acpatt/latin)** and supporting Drupal sites.
          * Grounding in **[C# programming](https://gitlab.com/acpatt/diffify)** and .NET Core APIs through supporting API instances.
          * Some experience in PHP form development and debugging in the context of Drupal applications.  
          * Competent in **[bash](https://gitlab.com/the-junkyard/cv/-/wikis/uploads/e6b7e15ddc82fd619a852aec904d4338/bash.sh)** **[scripting](https://gitlab.com/the-junkyard/cv/-/wikis/uploads/b40342aeb0a2ea720d6621c021093821/scriptiojg.sh)**, as well as experience in **[Powershell](https://gitlab.com/the-junkyard/cv/-/wikis/uploads/e8acc7cce501c890d040a09dfabdabfa/My_libraries_script.ps1)** and a grounding in **[Python scripting](https://gitlab.com/acpatt/forest-of-doom/-/blob/master/source.py)**
          * Experience of R package development, Rmarkdown reporting and Shiny app development.
          * Confident using Gitlab, Git version control generally and a grounding in Microsoft TFS.
      - title: Testing
        content: |
          * Some experience writing and running **[Cypress UI tests](https://gitlab.com/north-yorkshire-county-council/cypress)** for web applications
          * Experience with accessibility testing, as well as building a **[PoC](https://accessibilibot.acpatt.com/)** for an **[accessibility testing application](https://gitlab.com/nycc1/ny-a11y)** for NYCC using open-source tools.
          * Competent testing Ansible roles as well as integrating those tests into the deployment pipeline.
          * Experience having written the test strategy of a large multi-faceted system, detailing performance, User acceptance, integration, security and functional testing requirements, scope and approach. 
          * Proficient at writing test cases and **[scripts with cucumber/gherkin](https://gitlab.com/north-yorkshire-county-council/cypress/-/issues)**, having designed NYCC's **[user story and acceptance criteria template](https://gitlab.com/north-yorkshire-county-council/cypress/-/blob/master/.gitlab/issue_templates/user%20story.md)** to facilitate future integration with cucumber & cypress.
      - title: Monitoring
        content: |
          * Competent user of PRTG for network monitoring and reporting. as well as some experience with use of Zabbix for same.
          * Competent user of Kibana, Elasticsearch, Logstash for log analysis and collation. 
          * Proficient in log analysis as a debugging aid.
      - title: Compute
        content: |
          * Physical
              * Experience racking servers, installing network equipment and patching.
          * Operating systems
              * Very comfortable administering Debian/Ubuntu server and desktop environments, and confident using and administering RHEL-based systems. 
              * Confident using Windows server, as well as Active Directory and GPO administration.
              * Experience using MacOS and BSD-based systems. 
          * Virtualisation
              * 1 year experience building, managing and supporting ESXI clusters in an enterprise environment
              * 4 years experience building, managing and supporting Proxmox KVM clusters, as well as experience with **[automating VM infrastructure provisioning](https://gitlab.com/the-junkyard/ansible/-/blob/master/ansible/roles/linux/create_vm/tasks/main.yml)**
              * Experience deploying and developing infrastructure in DigitalOcean and Azure cloud platforms.
          * Containerisation
              * Confident '**[dockerizing](https://gitlab.com/the-junkyard/ansible_container)**' various applications, as well as developing, deploying and managing docker-based CMS deployment infrastructure. 
              * Some experience building and managing Kubernetes clusters, as well as deploying applications, scaling, etc.
              * Experience adminstering basic LXC deployments.
          * Building and configuring
              * Experience using **[Packer](https://gitlab.com/the-junkyard/wellington)** to build ubuntu images, as well as some experience with Windows images. 
              * Competent using and configuring Cloudinit for automated server configuration
              * Proficient in the use of **[ansible](https://gitlab.com/the-junkyard/ansible/-/tree/master/ansible/roles/linux/workstation)** for server configuration
      - title: Networking
        content: |
          * Firewalls/Network security
              * 1 year's experience configuring, installing and supporting Sonicwall and Palo Alto firewalls in enterprise and SME environments, as well as 3 years experience with PFSense.
              * Comfortable using Wireshark for packet analysis and network debugging. 
              * Solid foundational knowledge of information security and Cyberdefence.
          * Network management
              * Experienced with Linux bridges and OpenVSwitch for virtual networking, as well as 1 year's experience administering ZyXel and HP switches.
              * Competent using and debugging various network protocols including TCPIP, HTTP, TLS, IPSec, ARP, DHCP, DNS
      - title: Storage
        content: |
          * Competent working with NFS, SMB and ISCSI as well as experience working with NFS and a grounding in Ceph.
          * 1 year experience configuring and administering Freenas, Equalogic and Compellent storage arrays
          * Experience working with MySQL and MSSQL servers as a database user.

      - title: Automation - CICD, IaC, Config-as-code
        content: |
          * Proficient using Ansible for **[configuration automation](https://gitlab.com/the-junkyard/ansible)** and deployment.  
          * Experience building [Terraform](https://gitlab.com/acpatt/terraform) **[manifests](https://gitlab.com/the-junkyard/meta)** and using terraform to **[provision network](https://gitlab.com/acpatt/terraform/-/tree/master/devnet_scripts)** and compute infrastructure. 
          * Proficient with **[Gitlab-CI](https://gitlab.com/the-junkyard/ansible/-/blob/master/.gitlab-ci.yml)** for **[build](https://gitlab.com/the-junkyard/ansible_container/-/blob/master/.gitlab-ci.yml)**, **[test](https://gitlab.com/nycc1/ny-a11y/-/blob/develop/.gitlab-ci.yml)**, **[provisioning](https://gitlab.com/the-junkyard/meta/-/blob/master/.gitlab-ci.yml)** and deployment of various applications.
      - title: Cloud
        content: |
          * Some experience using Azure PowerApps and Azure virtual agents
          * Experience working with Logic-Apps for process automation. 
          * Some experience of Azure virtual machines, **[webapps](https://gitlab.com/acpatt/terraform/-/blob/master/eadwig_scripts/eadwig.tf#L108)** and **[virtual network resources](https://gitlab.com/acpatt/terraform/-/blob/master/devnet_scripts/devnet.tf)**.
      - title: Web
        content: |
          * Over 1 year experience building, deploying and supporting Drupal 7 and 8 sites for both internal and external clients.
          * Experience writing, debugging and supporting HTML/CSS code, working to establshed **[corporate design patterns](https://design-patterns.acpatt.com/)**.
          * Over 1 year experience managing apache webservers, php-fpm and mysql database instances.
          * Some experience generating and managing websites using the **[Jekyll](https://gitlab.com/the-junkyard/cv)** and **[Hugo](https://www.jumpingrivers.com/)** **[static site generator](https://gitlab.com/nycc1/ny-a11y/-/blob/develop/.gitlab-ci.yml#L55)**.

  - title: Soft
    accordion: 
      - title: Planning, organisation and working to deadlines
        content: |
          * Experience of successfully running datacentre disaster recovery testing on short timescales
          * Experience architecting, developing and deploying multiple infrastructure projects within the timescales set out, alongside multiple parallel projects and support.
          * Comfortable using Kanban for task management and process flow.
      - title: Coaching, Mentoring and Delivering training
        content: |
          * Experience of providing mentoring and coaching support in new technologies and network diagnostics, particularly by supporting work experience placements to grow their aptitude in analytical thinking and problem solving.
          * Confident writing delivering training packages to an audience of various levels of technical aptitude as a result of having trained multiple sets of clients on administration of systems built in my current position. 
          * Undertaken mentoring role for university undergraduate.
      - title: Written and verbal communication
        content: |
          * Confident recording and documenting internal processes and systems as a commitment to continuous improvement. 
          * Experience presenting solutions clearly, taking a solution-orientated approach to collaborative working. 
          * Skilled in documenting requirements, user stories and acceptance criteria.
          * Confident in service desk work, providing quality customer service on first, second and third line support for infrastructure, network and applications
          * Experience in pre- and post-sales consultation with clients.
          * Experience writing pre- and post-research reports on new technologies and candidate systems. 
      - title: Collaboration and co-ordination
        content: |
          * Some experience working as part of a scrum team, as well as experience and education into agile methodology. 
          * Active Participation in collaborative development 'Hacks' with a good level of collaboration within the team. 
          * Experience coordinating the completion of the deployment and back-out plan for a large multi-faceted system among multiple team members.

  - heading: Employment and education history
    title: Junior Data Engineer - Jumping Rivers LTD - (November 2020 - December 2020)
    description: |
      * Architecting, developing and deploying production infrastructure environments for multiple clients.
      * Rolling out automated testing throughout the infrastructure code estate.
  - title: Graduate Trainee - North yorkshire county council - (May 2019 - November 2020)
    description: |
      * Conducting research into cloud technologies, infrastructure automation, CI/CD technologies and test-automation. 
      * Building CI-pipelines for existing Drupal sites, as well as for C# APIs, using Ansible, Docker-compose, Deployer and MSbuild
      * Providing input and guidance on testing policy and individual system test strategies
      * Implementing agile requirements gathering/testing framework for project management and business change functions. 
      * Codifying server configuration using Ansible.

  - title: Analyst Programmer - North yorkshire county council - (October 2018 - May 2019)
    description: |
      * Working with Drupal, Elasticsearch and Apache Solr search engines, building and supporting multiple Drupal sites.
      * Scripting application installation with Bash and Gitlab-CI.
      * Supporting and documenting existing systems in order to facilitate product support

  - title: Systems Engineer - Keyfort Limited - (June 2017 - October 2018)
    description: |
      * Firewalling, switching and routing advanced concepts and administration, using a variety of firewall and switch hardware and software.
      * Network, server and storage administration, along with some experience with Docker and Microsoft Azure virtual networking.
      * Project and people management, having overseen fairly large customer and internal projects.
      * First, second and third line technical support, liaising directly with customers as well as with a team of engineers to resolve issues with a large variety of technologies.

  - title: Summer Placement, British Cyber-Security Organization (June 2016 - August 2016)
    description: |
      * Learning network protocols and getting practical experience using network diagnostic tools, courses in low level OS, Secure Coding in C++ and C# and practical cryptography. Cyber attack vectors such as SQL injection, XSS and SSRF/CSRF. 
      * Electronics and hardware hacking, using I2C & UART interfaces to interact with and alter embedded systems
      * Information security education, OSINT And information assurance crash courses.

  - title: Undergraduate degree 
    description: "* BSc (Hons) Theoretical Physics and Mathematics – Lancaster University - Classification 2:2"

layout: cv
---
