# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "2.18.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:DcHmM5XmjwrCCPqIdBRvYDrth5hjk6ldjsS94szjjJc=",
    "zh:4121da1ad26081552e3a648e94c89df96e246d50c6e307fe5eba586664691de0",
    "zh:4212865eeb42f491d3409f1b9edbb508dbc781a12144c4cb157a8057965144fb",
    "zh:4965fad90d5caf7917e0f7617d76a5d3419ca3f003e408f6e58af5e53f20b1ba",
    "zh:59d5dedb2b9c9b0a3fc5ad07fce4b1aefeaef5229dbe510e7f0f9f99bbb448aa",
    "zh:6746bfa2cfe6005b64286ccf9fcc5b25d1dc29d1448fe9b4f9acf7d3f7f05f79",
    "zh:78dd4811b35ea04f0ab11a0c7c600e8fe7f30e7645d8fc60d1d02272fa85568b",
    "zh:c7a7adf710bbf686d879825428f9ba92ec35fcb44742ffac5ea9b9538c43a19a",
    "zh:cd8827681b957a9a28cb8139414fd8430f228ff736be251c32ae26d8b146bfad",
    "zh:cf398859858618b5569b2ad3e84ee5550836b70083b1e7bcf3ba8398ff06e247",
    "zh:eb11da2096aea02c792dfe1a5e3605e711102401d03a722b8ae16223245e7f70",
    "zh:fc8d289e98dfa3e846b2c737cfdd821a6e1836a062e5453265d1dbb1e35433f1",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version = "3.5.0"
  hashes = [
    "h1:666HgCnsx/kDhHvND6rVll/HJ82jKWT/qqbNTDHKotQ=",
    "zh:20a7e10d88020a88f1aedd93d1de9a70efcc192f5c630df6a7818c4c3e501ac5",
    "zh:30c2f168869e7b243c844610bba5964de4acec9d0bb1255fc1ee6b97645dc398",
    "zh:4429f353f907f496997590113f1552db153d1b5793a64b06dd30ddd69e4c5c9b",
    "zh:9ab8c83053e8af0722e2aa04bc676a40543cc12f82e250d711bfaf2a791b58b1",
    "zh:a51dacd1f2022ac3edada0541327b87a7886ad981c32d29fd0bb48139a0b1360",
    "zh:ab568a4a19e272f2ead4d6e4f2a8ae61a18a952c92de741dfa6b1f501a18d30c",
    "zh:c7a7440c9b4698524506a6cbf8c8daf0dae32af3e245882b1ae2217c5a53bb9d",
    "zh:cc344adba161de020c677f7a6e233d829ffdfcd80ca154633caa5b5f23735346",
    "zh:ce6f02db7da21c308e824ea3d051e182f310fdba74fd4d40fedceb539b97daed",
    "zh:e44c8cde080e8690533623d4c56d9070cb4c720d12774e341c5b75c20291cd2d",
    "zh:ebdc5b7c23f3a0be2776402782312277cbd5ec5b54b8302eb64934e96e6c9a28",
  ]
}

provider "registry.terraform.io/louy/uptimerobot" {
  version     = "0.5.1"
  constraints = "0.5.1"
  hashes = [
    "h1:Oc5L04pt+5NJ8n47K7tVHbqFmuobW/x5zaF5sPGCQwE=",
    "zh:0cab4092125cf524a2e5caf74a4e90f230734b20bbceb20e51ffb82f2fcc90fc",
    "zh:130d9d9ddb2f9070d7846066d424d785a624472a25b413382bf6e80ad4754158",
    "zh:29c1ff142ec76adb751cf1e99b2c7bd9ba5f17b97bfcce5954043db9c4a83039",
    "zh:2ade3d4b911bcceb795b009105dfd58c40d27d06c2ea7402159a76559e86dd25",
    "zh:474d83e659597ee143d09100787bb35daf05dc2a0b3fbfcb8159792143b8c598",
    "zh:62694db2f947bafb00532a5b961067eb488005061e684eb74148bcbf80805270",
    "zh:6a4e423519a331328764ba0790eb79c63882e77aa8ffdf811a319b5efaf7034e",
    "zh:7e7f7c16f54690f84fcb6d1a90848bd51fc7b2eb2cc23bfa0583e18a80157393",
    "zh:a4a63a32ac0bbe0d11fda36768c824698db4c2378cbfdf5522276fe996621864",
    "zh:af3062664c5c4ec012af48dea769318dcb8ef77cbb59f15d8b6c252fb9adda85",
    "zh:de555bac4bd86d17e7b5592ec22a6db8d1496470d3dab4fa286a86e44bdad991",
  ]
}
