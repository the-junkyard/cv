terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/18855552/terraform/state/cv"
    lock_address = "https://gitlab.com/api/v4/projects/18855552/terraform/state/cv/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/18855552/terraform/state/cv/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"

  }
}