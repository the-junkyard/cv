---



# !!
# !!
# This site can be viewed at https://cv.acpatt.com
# !!
# !!


sections:
  - title: Who I am.
    description: |
      Hi, I'm Andrew. In the last 5 years I've gone from achieving an undergraduate degree in Physics and Mathematics, to completing an internship focused on cybersecurity and InfoSec, to a roles focused on server and network engineering, C#/PHP programming and now IaC/Config-as-code and automation work. Alongside building skills within my employment, for the last four years my hobby has been creating, deploying and managing virtual infrastructure.

      I am a very competent communicator, both spoken and in writing - I greatly enjoy engaging in coaching, educating and discussions about technologies and concepts. I have written and delivered training to multiple audiences of varying skill levels and this is something i'd be enthusiastic to pursue.

      In my current position, I have increasingly been asked to be involved in providing infrastructure services for our Data & Intelligence team. I have found the challenge of rapidly developing my understanding of unfamiliar technologies, deploying and configuring appropriate infrastructure solutions to be very rewarding. Working closely with my data-driven colleagues has piqued my interest in the world of data and analytics; In my next role I hope to broaden my DataOps skillset, increase my knowledge of data technologies and collaborate with colleagues and clients to deliver highly performing services. I believe that the role of Data engineer will allow me to realise these goals. I hope you will consider me for this position.

  - heading: Key Skills
    title: Technical
    accordion: 
      - title: Programming & Scripting
        content: |
          * Competent in **[bash](https://gitlab.com/the-junkyard/cv/-/wikis/uploads/e6b7e15ddc82fd619a852aec904d4338/bash.sh)** **[scripting](https://gitlab.com/the-junkyard/cv/-/wikis/uploads/b40342aeb0a2ea720d6621c021093821/scriptiojg.sh)**, as well as experience in **[Powershell](https://gitlab.com/the-junkyard/cv/-/wikis/uploads/e8acc7cce501c890d040a09dfabdabfa/My_libraries_script.ps1)** and a grounding in **[Python scripting](https://gitlab.com/acpatt/forest-of-doom/-/blob/master/source.py)**
          * Some experience of R programming during a Mathematics degree.
          * Competent user of Git version control generally and a grounding in TFS server.
          * Grounding in Javascript and NodeJS development through **[collaborating on software projects](https://gitlab.com/acpatt/latin)**.
      - title: Automation - CICD, IaC, Config-as-code
        content: |
          * Proficient using Ansible for **[configuration automation](https://gitlab.com/the-junkyard/ansible)** and deployment.  
          * Experience building [Terraform](https://gitlab.com/acpatt/terraform) **[manifests](https://gitlab.com/the-junkyard/meta)** and using terraform to **[provision network](https://gitlab.com/acpatt/terraform/-/tree/master/devnet_scripts)** and compute infrastructure. 
          * Proficient with **[Gitlab-CI](https://gitlab.com/the-junkyard/ansible/-/blob/master/.gitlab-ci.yml)** for **[build](https://gitlab.com/the-junkyard/ansible_container/-/blob/master/.gitlab-ci.yml)**, **[test](https://gitlab.com/nycc1/ny-a11y/-/blob/develop/.gitlab-ci.yml)**, **[provisioning](https://gitlab.com/the-junkyard/meta/-/blob/master/.gitlab-ci.yml)** and deployment of various applications. 
      - title: Compute
        content: |
          * Confident administering Debian/Ubuntu server and desktop environments, and competent using and administering RHEL-based systems. 
          * Confident using Windows server, as well as Active Directory and GPO administration.
          * 1 year experience building, managing and supporting ESXI clusters in an enterprise environment
          * Confident '**[dockerizing](https://gitlab.com/the-junkyard/ansible_container)**' various applications, as well as developing, deploying and managing the docker-based NYCC Drupal deployment infrastructure. 
          * Experience using **[Packer](https://gitlab.com/the-junkyard/wellington)** to build ubuntu images, as well as some experience with Windows images. 
          * Competent using and configuring Cloudinit for automated server configuration
          * Experience of using **[ansible](https://gitlab.com/the-junkyard/ansible/-/tree/master/ansible/roles/linux/workstation)** for server configuration
          * Experience with **[automating VM infrastructure provisioning](https://gitlab.com/the-junkyard/ansible/-/blob/master/ansible/roles/linux/create_vm/tasks/main.yml)**
      - title: Web
        content: |
          * Experience writing, debugging and supporting HTML/CSS code, working to establshed **[corporate design patterns](https://design-patterns.acpatt.com/)**.
          * Over 1 year experience managing apache webservers, php-fpm and mysql database instances.
          * Some experience working with **[Jekyll](https://gitlab.com/the-junkyard/cv)** **[static site generator](https://gitlab.com/nycc1/ny-a11y/-/blob/develop/.gitlab-ci.yml#L55)**.
      - title: Monitoring
        content: |
          * Competent user of Kibana, Elasticsearch, Logstash for log analysis and collation. 
          * Proficient in log analysis as a debugging aid.
      - title: Networking
        content: |
          * Solid foundational knowledge of information security and Cyberdefence.
          * Competent using and debugging various network protocols including TCPIP, HTTP, TLS, IPSec, ARP, DHCP, DNS
      - title: Cloud
        content: |
          * Experience working with Logic-Apps for process automation. 
          * Some experience of Azure virtual machines, **[webapps](https://gitlab.com/acpatt/terraform/-/blob/master/eadwig_scripts/eadwig.tf#L108)** and **[virtual network resources](https://gitlab.com/acpatt/terraform/-/blob/master/devnet_scripts/devnet.tf)**.

  - title: Soft
    accordion: 
      - title: Planning, organisation and working to deadlines
        content: |
          * Experience of successfully running datacentre disaster recovery testing on short timescales
          * Experience architecting,  developing and deploying multiple infrastructure projects within the timescales set out, alongside multiple parallel projects and support.
          * Comfortable using Kanban for task management and process flow.
      - title: Coaching, Mentoring and Delivering training
        content: |
          * Experience of providing mentoring and coaching support in new technologies and network diagnostics, particularly by **[mentoring work experience placements](https://gitlab.com/north-yorkshire-county-council/assessment-task/-/wikis/home)** to grow their aptitude in analytical thinking and problem solving.
          * Confident writing delivering training packages to an audience of various levels of technical aptitude as a result of having trained multiple sets of clients on administration of systems built in my current position. 
          * Undertaken mentoring role for university undergraduate.
      - title: Collaboration and co-ordination
        content: |
          * Some experience working as part of a scrum team, as well as experience and education into agile methodology. 
          * Experience coordinating the completion of the deployment and back-out plan for a large multi-faceted system among multiple team members.

  - heading: Employment history
    title: Graduate Trainee - North Yorkshire county council - (May 2019 - Present)
    description: |
      * Conducting research into cloud technologies, infrastructure automation, CI/CD technologies and test-automation. 
      * Building CI-pipelines for existing Drupal sites, as well as for C# APIs, using Ansible, Docker-compose, Deployer and MSbuild
      * Providing input and guidance on testing policy and individual system test strategies
      * Codifying server configuration using Ansible.

  - title: Analyst Programmer - North Yorkshire county council - (October 2018 - April 2019)
    description: |
      * Working with Drupal, Elasticsearch and Apache Solr search engines, building and supporting multiple Drupal sites.
      * Scripting application installation with Bash and Gitlab-CI.

  - title: Systems Engineer - Keyfort Limited - (June 2017 - October 2018)
    description: |
      * Firewalling, switching and routing advanced concepts and administration, using a variety of firewall and switch hardware and software.
      * Network, server and storage administration, along with some experience with Docker and Microsoft Azure virtual networking.
      * Project and people management, having overseen fairly large customer and internal projects.
      * First, second and third line technical support, liaising directly with customers as well as with a team of engineers to resolve issues with a large variety of technologies.

  - title: Summer Placement, British Cyber-Security Organization (June 2016 - August 2016)
    description: |
      * Learning network protocols and getting practical experience using network diagnostic tools, courses in low level OS, Secure Coding in C++ and C# and practical cryptography.
      * Information security education, OSINT And information assurance crash courses.

  - heading: Education
    title: Undergraduate degree
    description: "* BSc (Hons) Theoretical Physics and Mathematics – Lancaster University - Classification 2:2"

  - heading: More info
    description: |
      Please see [my full CV page](https://cv.acpatt.com/) for more details of my experience and clarity on how I **[define my competency levels](https://cv.acpatt.com/#skillset)**. 
      
      Thanks for reading - Andrew

layout: cv
---